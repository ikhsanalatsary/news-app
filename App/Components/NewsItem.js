import React, { PureComponent } from 'react'
import { Image, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { Text, Card, CardItem, Left, Body, Right, Title } from 'native-base'
import moment from 'moment'
import Icon from 'react-native-vector-icons/Ionicons'
import { Colors, Images } from '../Themes'
// import styles from './Styles/NewsItemStyle'

export default class NewsItem extends PureComponent {
  iconOpen = ''

  async componentWillMount () {
    this.iconOpen = await Icon.getImageSource('md-open', 30)
  }

  _handlePress = () => {
    this.props.navigator.push({
      screen: 'ikhsanapp.Webview', // unique ID registered with Navigation.registerScreen
      title: this.props.article.title, // navigation bar title of the pushed screen (optional)
      // titleImage: require('../../img/my_image.png'), // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
      passProps: {
        url: this.props.article.url,
      }, // Object that will be passed as props to the pushed screen (optional)
      animated: true, // does the push have transition animation or does it happen immediately (optional)
      animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the push have different transition animation (optional)
      navigatorButtons: {
        rightButtons: [
          {
            icon: this.iconOpen,
            id: 'openBrowser',
          },
        ],
      },
    })
  }

  render () {
    // console.log(this.props);
    const { article } = this.props
    return (
      <TouchableOpacity onPress={this._handlePress} activeOpacity={0.8}>
        <Card>
          <CardItem>
            <Body>
              <Title style={{color: Colors.black}}>{article.title}</Title>
            </Body>
          </CardItem>
          <CardItem cardBody>
            <Image source={article.urlToImage ? {uri: article.urlToImage} : Images.placeholderImage} style={{height: 200, width: null, flex: 1}} />
          </CardItem>
          <CardItem>
            <Body>
              <Text>
                {article.description}
              </Text>
            </Body>
          </CardItem>
          <CardItem>
            <Left>
              <Text note>{article.author}</Text>
            </Left>
            <Right>
              <Text note>{moment(article.publishedAt).format('LL')}</Text>
            </Right>
          </CardItem>
        </Card>
      </TouchableOpacity>
    )
  }
}

NewsItem.propTypes = {
  article: PropTypes.object,
  navigator: PropTypes.object,
}
