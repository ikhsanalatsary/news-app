import React, { PureComponent } from 'react'
import { Platform } from 'react-native'
import PropTypes from 'prop-types'
import { Text, Body, ListItem, Thumbnail, Badge } from 'native-base'
import { Colors, Images } from '../Themes'
// import styles from './Styles/NewsItemStyle'

export default class SourceItem extends PureComponent {
  state = {
    imageError: false,
  }

  _onErrorImage = () => {
    this.setState({ imageError: true })
  }

  render () {
    const { item } = this.props
    const uri = `https://besticon-demo.herokuapp.com/icon?url=${item.url}&size=70..120..200`
    return (
      <ListItem onPress={this.props.handleRoute} style={{ alignItems: 'flex-start' }}>
        <Thumbnail square size={80} source={this.state.imageError ? Images.placeholderImage : { uri }} onError={this._onErrorImage} />
        <Body style={{flex: 3}}>
          <Text style={{color: Colors.snow}}>{item.name}</Text>
          <Text note>{item.description}</Text>
          <Badge danger style={{marginLeft: Platform.select({ android: 15, ios: 12 }), marginTop: 5, borderRadius: 2}}>
            <Text style={{fontSize: 12, margin: 2, color: Colors.snow}}>{item.category}</Text>
          </Badge>
        </Body>
      </ListItem>
    )
  }
}

SourceItem.propTypes = {
  item: PropTypes.object,
  handleRoute: PropTypes.func,
}
