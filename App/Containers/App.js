import '../Config'
// import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { Navigation } from 'react-native-navigation'
// import RootContainer from './RootContainer'
import createStore from '../Redux'
import { registerScreens } from '../Navigation/ScreenNavigation'
import { ApplicationStyles, Images } from '../Themes'
import RehydrationServices from '../Services/RehydrationServices'
import ReduxPersist from '../Config/ReduxPersist'
import StartupActions from '../Redux/StartupRedux'

// create our store
const store = createStore()

registerScreens(store, Provider)

const App = {
  _initApp () {
    Navigation.startSingleScreenApp({
      screen: {
        screen: 'ikhsanapp.Launch',
        title: 'Newsapi.org',
        navigatorStyle: ApplicationStyles.navigatorStyle,
        leftButtons: [
          {
            id: 'sideMenu',
            icon: Images.home,
          },
        ],
      },
      appStyle: {
        orientation: 'portrait', // Sets a specific orientation to the entire app. Default: 'auto'. Supported values: 'auto', 'landscape', 'portrait'
      },
    })
    return this
  },
  start () {
    // configure persistStore and check reducer version number
    if (ReduxPersist.active) {
      RehydrationServices.updateReducers(store, () => {
        registerScreens(store, Provider)
        store.dispatch(StartupActions.startup())
        this._initApp()
      })
    } else {
      registerScreens(store, Provider)
      store.dispatch(StartupActions.startup())
      this._initApp()
    }
    return this
  },
}

/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */
// class App extends Component {
//   render () {
//     return (
//       <Provider store={store}>
//         <RootContainer />
//       </Provider>
//     )
//   }
// }

export default Object.create(App)
