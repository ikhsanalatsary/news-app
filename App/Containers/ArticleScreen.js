import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Dimensions, Linking, WebView } from 'react-native'
import { ApplicationStyles, Colors } from '../Themes'

class ArticleScreen extends Component {
  static navigatorStyle = ApplicationStyles.navigatorStyle

  constructor (props) {
    super(props)

    this.props.navigator.setOnNavigatorEvent(this._onNavigatorEvent)
  }

  _onNavigatorEvent = (event) => {
    if (event.type === 'NavBarButtonPress') {
      if (event.id === 'openBrowser') {
        Linking.openURL(this.props.url)
      }
    }
  }

  render () {
    return (
      <WebView
        domStorageEnabled
        startInLoadingState
        scalesPageToFit
        allowsInlineMediaPlayback
        userAgent="ikhsanapp"
        source={{ uri: this.props.url }}
        onLoad={evt => {}}
        style={{
          height: Dimensions.get('window').height,
          width: Dimensions.get('window').width,
          backgroundColor: Colors.snow,
        }}
      />
    )
  }
}

ArticleScreen.propTypes = {
  url: PropTypes.string,
  navigator: PropTypes.object,
}

export default ArticleScreen
