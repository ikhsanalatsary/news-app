import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { ScrollView, Image, View, FlatList, RefreshControl } from 'react-native'
import { Text, Card, CardItem, Body, Spinner } from 'native-base'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { Images, Colors } from '../Themes'
import API from '../Services/Api'

import {
  selectEverythingLoading,
  selectEverythingData,
  selectEverythingError,
} from '../Selectors/EverythingSelectors'
import EverythingActions from '../Redux/EverythingRedux'

// Styles
import styles from './Styles/HeadlineScreenStyle'

// Component
import NewsItem from '../Components/NewsItem'

const api = API.create()
const CancelToken = API.CancelToken

class EverythingScreen extends PureComponent {
  cancel = null

  state = {
    data: [],
    refreshing: false,
    loading: false,
    currentPage: 1,
  }

  componentDidMount () {
    this.props.fetchData({
      sources: this.props.source,
      sortBy: 'publishedAt',
      page: 1,
    }, {
      cancelToken: new CancelToken((c) => {
        this.cancel = c
      }),
    })
  }

  componentWillUnmount () {
    if (this.cancel) {
      this.cancel('Operation canceled by the user')
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.data && nextProps.data.articles) {
      this.setState({ data: nextProps.data.articles, refreshing: false, loading: false })
    }
  }

  _keyExtractor = (item, index) => `${index}`

  _renderItem = ({ item }) => <NewsItem article={item} navigator={this.props.navigator} />

  _onRefresh = () => {
    this.setState(
      {
        refreshing: true,
      },
      () => {
        this.props.fetchData({
          sources: this.props.source,
          sortBy: 'publishedAt',
          page: 1,
        }, {
          cancelToken: new CancelToken((c) => {
            this.cancel = c
          }),
        }, false, false)
      }
    )
  }

  async _retrieveNextPage () {
    if (this.props.data && !this.state.loading) {
      if (this.state.currentPage !== this.props.data.totalResults) {
        this.setState({
          currentPage: this.state.currentPage + 1,
          loading: true,
        })

        let page
        if (this.state.currentPage === 1) {
          page = 2
          this.setState({ currentPage: 2, loading: true })
        } else {
          page = this.state.currentPage + 1
        }
        try {
          const data = [...this.state.data]
          const results = await api.getEverything({
            sources: this.props.source,
            sortBy: 'publishedAt',
            page,
          }, {
            cancelToken: new CancelToken((c) => {
              this.cancel = c
            }),
          })
          if (results && results.data) {
            // eslint-disable-next-line
            let newData = results.data.articles.map((item, index) => data.push(item))
            this.setState({
              data,
              loading: false,
            })
          }
        } catch (e) {}
      }
    }
  }

  _renderFooter = () => {
    if (!this.state.loading) return null
    return <Spinner color={Colors.accent} />
  }

  render () {
    const { error, fetching } = this.props
    const { data, refreshing } = this.state
    const refreshControl = (
      <RefreshControl
        refreshing={refreshing}
        onRefresh={this._onRefresh}
        colors={[Colors.accent]}
        tintColor={Colors.accent}
        title="Pull to refresh"
        titleColor={Colors.accent}
      />
    )
    let content = null
    if (fetching) {
      content = <Spinner color={Colors.accent} />
    } else if (data) {
      content = (
        <FlatList
          scrollEventThrottle={0}
          onEndReached={type => this._retrieveNextPage()}
          onEndReachedThreshold={1200}
          data={data}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          refreshControl={refreshControl}
          ListFooterComponent={this._renderFooter}
          style={styles.container}
        />
      )
    } else {
      content = (
        <ScrollView style={styles.container}>
          <Card style={styles.centered}>
            <CardItem>
              <Body>
                <Text>{error}</Text>
              </Body>
            </CardItem>
          </Card>
        </ScrollView>
      )
    }
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode="stretch" />
        {content}
      </View>
    )
  }
}
EverythingScreen.propTypes = {
  data: PropTypes.object,
  error: PropTypes.string,
  source: PropTypes.string,
  fetchData: PropTypes.func,
  fetching: PropTypes.bool,
  navigator: PropTypes.object,
}

const mapStateToProps = createStructuredSelector({
  fetching: selectEverythingLoading(),
  data: selectEverythingData(),
  error: selectEverythingError(),
})

const mapDispatchToProps = dispatch => ({
  fetchData: (params, config, fetching = true, reset = true) => dispatch(EverythingActions.everythingRequest(params, config, fetching, reset)),
})

export default connect(mapStateToProps, mapDispatchToProps)(EverythingScreen)
