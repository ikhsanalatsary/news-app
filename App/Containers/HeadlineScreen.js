import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { ScrollView, Image, View, RefreshControl } from 'react-native'
import { Text, Card, CardItem, Body, Spinner } from 'native-base'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { Images, Colors } from '../Themes'

import {
  selectHeadlineLoading,
  selectHeadlineData,
  selectHeadlineError,
} from '../Selectors/HeadlineSelectors'
import HeadlineActions from '../Redux/HeadlineRedux'
import API from '../Services/Api'

// Styles
import styles from './Styles/HeadlineScreenStyle'

// Component
import NewsItem from '../Components/NewsItem'

const CancelToken = API.CancelToken

class HeadlineScreen extends PureComponent {
  cancel = null

  state = {
    data: [],
    refreshing: false,
  }

  componentDidMount () {
    this.props.fetchData({
      sources: this.props.source,
    }, {
      cancelToken: new CancelToken((c) => {
        this.cancel = c
      }),
    })
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.data && nextProps.data.articles) {
      this.setState({ data: nextProps.data.articles, refreshing: false })
    }
  }

  componentWillUnmount () {
    if (this.cancel) {
      this.cancel()
    }
  }

  _onRefresh = () => {
    this.setState(
      {
        refreshing: true,
      },
      () => {
        this.props.fetchData({ sources: this.props.source }, {
          cancelToken: new CancelToken((c) => {
            this.cancel = c
          }),
        }, false)
      }
    )
  }

  listArticle = (article, i) => {
    return <NewsItem article={article} key={i} navigator={this.props.navigator} />
  }

  render () {
    const { error, fetching } = this.props
    const { data, refreshing } = this.state
    const refreshControl = (
      <RefreshControl
        refreshing={refreshing}
        onRefresh={this._onRefresh}
        colors={[Colors.accent]}
        tintColor={Colors.accent}
        title="Pull to refresh"
        titleColor={Colors.accent}
      />
    )
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode="stretch" />
        <ScrollView style={styles.container} refreshControl={refreshControl}>
          {fetching && <Spinner color={Colors.accent} />}
          {data.length > 0 && data.map(this.listArticle)}
          {error && (
            <Card style={styles.centered}>
              <CardItem>
                <Body>
                  <Text>{error}</Text>
                </Body>
              </CardItem>
            </Card>
          )}
        </ScrollView>
      </View>
    )
  }
}

HeadlineScreen.propTypes = {
  data: PropTypes.object,
  error: PropTypes.string,
  source: PropTypes.string,
  fetchData: PropTypes.func,
  fetching: PropTypes.bool,
  navigator: PropTypes.object,
}

const mapStateToProps = createStructuredSelector({
  fetching: selectHeadlineLoading(),
  data: selectHeadlineData(),
  error: selectHeadlineError(),
})

const mapDispatchToProps = dispatch => ({
  fetchData: (params, config, fetching = true) => dispatch(HeadlineActions.headlineRequest(params, config, fetching)),
})

export default connect(mapStateToProps, mapDispatchToProps)(HeadlineScreen)
