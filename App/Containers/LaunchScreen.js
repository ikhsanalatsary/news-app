import React from 'react'
import PropTypes from 'prop-types'
import { View, FlatList, RefreshControl } from 'react-native'
import { Spinner } from 'native-base'
import Icon from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { Colors } from '../Themes'

import SourceActions from '../Redux/SourceRedux'
import {
  selectSourceData,
  selectSourceError,
  selectSourceLoading,
} from '../Selectors/SourceSelectors'

// Styles
import styles from './Styles/LaunchScreenStyles'

// Component
import SourceItem from '../Components/SourceItem'

class LaunchScreen extends React.Component {
  iconSearch = ''

  state = {
    data: [],
    refreshing: false,
  }

  async componentWillMount () {
    this.iconSearch = await Icon.getImageSource('md-search', 30)
  }

  componentDidMount () {
    if (this.props.data && this.props.data.sources) {
      this.setState({
        data: this.props.data.sources,
      })
    } else {
      this.props.fetchData({})
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({ refreshing: false })
    if (nextProps.data) {
      this.setState({
        data: nextProps.data.sources,
      })
    }
  }

  _handleRoute = (item) => () => {
    this.props.navigator.push({
      screen: 'ikhsanapp.News', // unique ID registered with Navigation.registerScreen
      title: item.name, // navigation bar title of the pushed screen (optional)
      subtitle: undefined, // navigation bar subtitle of the pushed screen (optional)
      // titleImage: require('../../img/my_image.png'), // iOS only. navigation bar title image instead of the title text of the pushed screen (optional)
      passProps: {
        source: item.id,
      }, // Object that will be passed as props to the pushed screen (optional)
      animated: true, // does the push have transition animation or does it happen immediately (optional)
      animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the push have different transition animation (optional)
      navigatorButtons: {
        rightButtons: [
          {
            icon: this.iconSearch,
            id: 'searchView',
          },
        ],
      },
    })
  }

  _keyExtractor = (item, index) => item.id

  _renderItem = ({ item }) => <SourceItem item={item} handleRoute={this._handleRoute(item)} />

  _onRefresh = () => {
    this.setState(
      {
        refreshing: true,
      },
      () => {
        this.props.fetchData({}, false)
      }
    )
  }

  render () {
    const { fetching } = this.props
    const { refreshing, data } = this.state
    const refreshControl = (
      <RefreshControl
        refreshing={refreshing}
        onRefresh={this._onRefresh}
        colors={[Colors.accent]}
        tintColor={Colors.accent}
        title="Pull to refresh"
        titleColor={Colors.accent}
      />
    )
    if (fetching) {
      return <View style={styles.container}><Spinner color={Colors.accent} /></View>
    } else if (data) {
      return <FlatList
        data={data}
        renderItem={this._renderItem}
        keyExtractor={this._keyExtractor}
        refreshControl={refreshControl}
        style={{backgroundColor: 'black'}}
      />
    }
    return null
  }
}

LaunchScreen.propTypes = {
  data: PropTypes.object,
  navigator: PropTypes.object,
  fetchData: PropTypes.func,
  fetching: PropTypes.bool,
}

const mapStateToProps = createStructuredSelector({
  fetching: selectSourceLoading(),
  data: selectSourceData(),
  error: selectSourceError(),
})

const mapDispatchToProps = dispatch => ({
  fetchData: (params, fetching = true) => dispatch(SourceActions.sourceRequest(params, fetching)),
})

export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)
