import React from 'react'
import PropTypes from 'prop-types'
import { Dimensions } from 'react-native'
import { TabViewAnimated, TabBar } from 'react-native-tab-view'
import { Colors, ApplicationStyles } from '../Themes'

// Styles
import styles from './Styles/NewsScreenStyles'

// Component
import Headline from './HeadlineScreen'
import Everything from './EverythingScreen'

class NewsScreen extends React.Component {
  static navigatorStyle = ApplicationStyles.navigatorStyle

  constructor (props) {
    super(props)

    this.state = {
      index: 0,
      routes: [
        { key: '1', title: 'Top Headline' },
        { key: '2', title: 'All' },
      ],
      loaded: false,
    }

    this.props.navigator.setOnNavigatorEvent(this._onNavigatorEvent)
  }

  _handleIndexChange = index => this.setState(_ => ({ index }))

  _onNavigatorEvent = (event) => {
    if (event.type === 'NavBarButtonPress') {
      if (event.id === 'searchView') {
        this.props.navigator.showModal({
          screen: 'ikhsanapp.Search',
          title: 'Search',
          passProps: {
            source: this.props.source,
          },
        })
      }
    }
  }

  _renderHeader = props => {
    return (
      <TabBar
        {...props}
        indicatorStyle={styles.indicator}
        style={styles.tabbar}
        labelStyle={styles.label}
        pressColor={Colors.fire}
        pressOpacity={0.5}
        scrollEnabled={false}
        useNativeDriver
      />
    )
  }

  _renderScene = state => {
    const { route } = state

    let selectedScene = (route, handler) => {
      if (!handler[route.key]) {
        return null
      }
      return handler[route.key]
    }

    return selectedScene(route, {
      '1': <Headline source={this.props.source} navigator={this.props.navigator} />,
      '2': <Everything source={this.props.source} navigator={this.props.navigator} />,
    })
  }

  render () {
    return (
      <TabViewAnimated
        useNativeDriver
        style={styles.tabContainer}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderHeader={this._renderHeader}
        onIndexChange={this._handleIndexChange}
        initialLayout={{
          height: 0,
          width: Dimensions.get('window').width,
        }}
      />
    )
  }
}

NewsScreen.propTypes = {
  source: PropTypes.string,
  navigator: PropTypes.object,
}

export default NewsScreen
