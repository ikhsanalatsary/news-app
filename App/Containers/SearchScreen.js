import React from 'react'
import PropTypes from 'prop-types'
import { FlatList, TextInput, View } from 'react-native'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { ApplicationStyles } from '../Themes'
import API from '../Services/Api'

import {
  selectEverythingLoading,
  selectEverythingData,
  selectEverythingError,
} from '../Selectors/EverythingSelectors'
import EverythingActions from '../Redux/EverythingRedux'

// Styles
import styles from './Styles/SearchScreenStyles'

// Component
import NewsItem from '../Components/NewsItem'

const api = API.create()
const CancelToken = API.CancelToken

class SearchScreen extends React.Component {
  static navigatorStyle = ApplicationStyles.navigatorStyle

  cancel = null

  constructor (props) {
    super(props)

    this.state = {
      isLoading: true,
      currentPage: 1,
      results: [],
      query: null,
    }

    this.props.navigator.setOnNavigatorEvent(this._onNavigatorEvent)
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.data) {
      this.setState({
        results: nextProps.data.articles,
        isLoading: false,
      })
    }
  }

  componentWillUnmount () {
    if (this.cancel) {
      this.cancel()
    }
    this.props.fetchData({
      sources: this.props.source,
      sortBy: 'publishedAt',
      page: 1,
    })
  }

  _handleTextInput = (event) => {
    const query = event.nativeEvent.text
    this.setState({ query })
    if (!query) this.setState({ query: '' })

    setTimeout(() => {
      if (query.length) {
        this.props.fetchData({
          sources: this.props.source,
          q: this.state.query,
          sortBy: 'publishedAt',
          page: 1,
        }, {
          cancelToken: new CancelToken((c) => {
            this.cancel = c
          }),
        })
      }
    }, 500)
  }

  _keyExtractor = (item, index) => `${index}`

  _onNavigatorEvent = (event) => {
    if (event.type === 'NavBarButtonPress') {
      if (event.id === 'close') {
        this.props.navigator.dismissModal()
      }
    }
  }

  _renderItem = ({ item }) => <NewsItem article={item} navigator={this.props.navigator} />

  _renderListView = () => {
    let listView
    if (this.state.query) {
      listView = (
        <FlatList
          scrollEventThrottle={0}
          onEndReached={type => this._retrieveNextPage()}
          onEndReachedThreshold={1200}
          data={this.state.results}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          style={{backgroundColor: 'black'}}
        />
      )
    } else {
      listView = <View />
    }

    return listView
  }

  async _retrieveNextPage () {
    if (this.props.data) {
      if (this.state.currentPage !== this.props.data.totalResults) {
        this.setState({
          currentPage: this.state.currentPage + 1,
        })

        let page
        if (this.state.currentPage === 1) {
          page = 2
          this.setState({ currentPage: 2 })
        } else {
          page = this.state.currentPage + 1
        }
        try {
          const data = [...this.state.results]
          const results = await api.getEverything({
            sources: this.props.source,
            sortBy: 'publishedAt',
            page,
          }, {
            cancelToken: new CancelToken((c) => {
              this.cancel = c
            }),
          })
          if (results && results.data) {
            // eslint-disable-next-line
            let newData = results.data.articles.map((item, index) => data.push(item))
            this.setState({
              results: data,
            })
          }
        } catch (e) {}
      }
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.searchbox}>
          <View style={styles.searchboxBorder}>
            <TextInput
              style={styles.textInput}
              autoFocus
              returnKeyType={'search'}
              value={this.state.query}
              onChange={this._handleTextInput}
              underlineColorAndroid="transparent"
            />
          </View>
        </View>
        { !this.state.isLoading && this._renderListView() }
      </View>
    )
  }
}

SearchScreen.propTypes = {
  data: PropTypes.object,
  source: PropTypes.string,
  fetchData: PropTypes.func,
  navigator: PropTypes.object,
}

const mapStateToProps = createStructuredSelector({
  fetching: selectEverythingLoading(),
  data: selectEverythingData(),
  error: selectEverythingError(),
})

const mapDispatchToProps = dispatch => ({
  fetchData: (params, config, fetching = true, reset = true) => dispatch(EverythingActions.everythingRequest(params, config, fetching, reset)),
})

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen)
