import React from "react";
import { DrawerNavigator } from "react-navigation";
import ArticleScreen from '../Containers/ArticleScreen'
import HeadlineScreen from '../Containers/HeadlineScreen'
import ListviewExample from "../Containers/ListviewExample";
import CardExample from "../Containers/CardExample";
import DrawerContent from "../Containers/DrawerContent";

import styles from "./Styles/NavigationStyles";

const NavigationDrawer = DrawerNavigator({
  ArticleScreen: { screen: ArticleScreen },
  HeadlineScreen: { screen: HeadlineScreen },
		ListviewExample: { screen: ListviewExample },
		CardExample: { screen: CardExample },
	},
	{
		initialRouteName: "ListviewExample",
		contentComponent: props => <DrawerContent {...props} />,
	}
);

export default NavigationDrawer;
