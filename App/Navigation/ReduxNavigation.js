// @flow
import React from 'react'
import { connect } from 'react-redux'
import AppNavigation from './AppNavigation'
import { addListener } from '../Redux/ReduxUtil';

// here is our redux-aware our smart component
class ReduxNavigation extends React.Component {
  render () {
    const { dispatch, nav } = this.props
    const navigation = {
      dispatch,
      state: nav,
      addListener,
    }

    return <AppNavigation navigation={navigation} />
  }
}

const mapStateToProps = state => ({ nav: state.nav })
export default connect(mapStateToProps)(ReduxNavigation)
