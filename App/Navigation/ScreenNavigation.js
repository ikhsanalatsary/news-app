import { Navigation } from 'react-native-navigation'
import Login from '../Containers/LoginScreen'
import LaunchScreen from '../Containers/LaunchScreen'
import NewsScreen from '../Containers/NewsScreen'
import SearchScreen from '../Containers/SearchScreen'
import ArticleScreen from '../Containers/ArticleScreen'
// import NavigationDrawer from "./NavigationDrawer";

export function registerScreens (store, Provider) {
  Navigation.registerComponent('ikhsanapp.Login', () => Login, store, Provider)
  Navigation.registerComponent('ikhsanapp.Launch', () => LaunchScreen, store, Provider)
  Navigation.registerComponent('ikhsanapp.News', () => NewsScreen, store, Provider)
  Navigation.registerComponent('ikhsanapp.Search', () => SearchScreen, store, Provider)
  Navigation.registerComponent('ikhsanapp.Webview', () => ArticleScreen, store, Provider)
}
