import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  everythingRequest: ['params', 'config', 'fetching', 'reset'],
  everythingSuccess: ['everythingData'],
  everythingFailure: ['error'],
})

export const EverythingTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  everythingData: null,
  error: null,
  fetching: false,
})

/* ------------- Reducers ------------- */

// we're attempting to login
export const request = (state, { fetching, reset, everythingData }) => {
  let data = everythingData
  if (reset) {
    data = null
  }
  return state.merge({ fetching, everythingData: data, error: null })
}

// we've successfully logged in
export const success = (state, { everythingData }) =>
  state.merge({ fetching: false, error: null, everythingData })

// we've had a problem logging in
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.EVERYTHING_REQUEST]: request,
  [Types.EVERYTHING_SUCCESS]: success,
  [Types.EVERYTHING_FAILURE]: failure,
})
