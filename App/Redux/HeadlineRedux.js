import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  headlineRequest: ['params', 'config', 'fetching'],
  headlineSuccess: ['headlineData'],
  headlineFailure: ['error'],
})

export const HeadlineTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  headlineData: null,
  error: null,
  fetching: false,
})

/* ------------- Reducers ------------- */

// we're attempting to login
export const request = (state, { fetching }) => state.merge({ fetching, error: null, headlineData: null })

// we've successfully logged in
export const success = (state, { headlineData }) =>
  state.merge({ fetching: false, error: null, headlineData })

// we've had a problem logging in
export const failure = (state, { error }) => state.merge({ fetching: false, error })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.HEADLINE_REQUEST]: request,
  [Types.HEADLINE_SUCCESS]: success,
  [Types.HEADLINE_FAILURE]: failure,
})
