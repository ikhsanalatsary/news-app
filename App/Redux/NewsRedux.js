import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  newsRequest: ['params'],
  newsSuccess: ['headlineData'],
  newsFailure: ['error'],
})

export const NewsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  newsData: null,
  error: null,
  fetching: false,
})

/* ------------- Reducers ------------- */

// we're attempting to login
export const request = (state) => state.merge({ fetching: true })

// we've successfully logged in
export const success = (state, { newsData }) =>
  state.merge({ fetching: false, error: null, newsData })

// we've had a problem logging in
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.NEWS_REQUEST]: request,
  [Types.NEWS_SUCCESS]: success,
  [Types.NEWS_FAILURE]: failure,
})
