import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  sourceRequest: ['params', 'fetching'],
  sourceSuccess: ['sourceData'],
  sourceFailure: ['error'],
})

export const SourceTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  sourceData: null,
  error: null,
  fetching: false,
})

/* ------------- Reducers ------------- */

// we're attempting to login
export const request = (state, { fetching }) => state.merge({ fetching })

// we've successfully logged in
export const success = (state, { sourceData }) =>
  state.merge({ fetching: false, error: null, sourceData })

// we've had a problem logging in
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SOURCE_REQUEST]: request,
  [Types.SOURCE_SUCCESS]: success,
  [Types.SOURCE_FAILURE]: failure,
})
