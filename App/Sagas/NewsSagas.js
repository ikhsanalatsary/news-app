import { all, call, put, takeLatest } from 'redux-saga/effects'
import { complement, isNil } from 'ramda'

import HeadlineActions, { HeadlineTypes } from '../Redux/HeadlineRedux'
import EverythingActions, { EverythingTypes } from '../Redux/EverythingRedux'
import NewsActions, { NewsTypes } from '../Redux/NewsRedux'
import SourceActions, { SourceTypes } from '../Redux/SourceRedux'

const isNotNil = complement(isNil)

export function * getHeadline (api, { params, config }) {
  try {
    // make the call to the api
    const response = yield call(api.getTopHeadline, params, config)
    if (isNotNil(response) && isNotNil(response.data)) {
      yield put(HeadlineActions.headlineSuccess(response.data))
    }
  } catch (e) {
    if (e.response && e.response.data) {
      yield put(HeadlineActions.headlineFailure(e.response.data.message))
    } else if (e.response && e.response.problem && !e.response.data) {
      yield put(HeadlineActions.headlineFailure(e.response.problem))
    } else {
      let err = new Error('Lost Connection')
      yield put(HeadlineActions.headlineFailure(err.message))
    }
  }
}
export function * getEverythingNews (api, { params, config }) {
  try {
    // make the call to the api
    const response = yield call(api.getEverything, params, config)
    if (isNotNil(response) && isNotNil(response.data)) {
      yield put(EverythingActions.everythingSuccess(response.data))
    }
  } catch (e) {
    if (e.response && e.response.data) {
      yield put(EverythingActions.everythingFailure(e.response.data.message))
    } else if (e.response && e.response.problem && !e.response.data) {
      yield put(HeadlineActions.headlineFailure(e.response.problem))
    } else {
      let err = new Error('Lost Connection')
      yield put(HeadlineActions.headlineFailure(err.message))
    }
  }
}
export function * getArticles (api, { params }) {
  try {
    // make the call to the api
    const response = yield call(api.getArticles, params)
    if (isNotNil(response) && isNotNil(response.data)) {
      yield put(NewsActions.newsSuccess(response.data))
    }
  } catch (e) {
    if (e.response && e.response.data) {
      yield put(NewsActions.newsFailure(e.response.data.message))
    } else if (e.response && e.response.problem && !e.response.data) {
      yield put(HeadlineActions.headlineFailure(e.response.problem))
    } else {
      let err = new Error('Lost Connection')
      yield put(HeadlineActions.headlineFailure(err.message))
    }
  }
}
export function * getSources (api, { params }) {
  try {
    // make the call to the api
    const response = yield call(api.getSources, params)
    if (isNotNil(response) && isNotNil(response.data)) {
      yield put(SourceActions.sourceSuccess(response.data))
    }
  } catch (e) {
    if (e.response && e.response.data) {
      yield put(SourceActions.sourceFailure(e.response.data.message))
    } else if (e.response && e.response.problem && !e.response.data) {
      yield put(HeadlineActions.headlineFailure(e.response.problem))
    } else {
      let err = new Error('Lost Connection')
      yield put(HeadlineActions.headlineFailure(err.message))
    }
  }
}

function * newsWatcher (api) {
  yield all([
    takeLatest(HeadlineTypes.HEADLINE_REQUEST, getHeadline, api),
    takeLatest(EverythingTypes.EVERYTHING_REQUEST, getEverythingNews, api),
    takeLatest(NewsTypes.NEWS_REQUEST, getArticles, api),
    takeLatest(SourceTypes.SOURCE_REQUEST, getSources, api),
  ])
}

export default newsWatcher
