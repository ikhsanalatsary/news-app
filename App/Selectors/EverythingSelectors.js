import { createSelector } from 'reselect'
import { has } from 'ramda'
const isImmutable = has('asMutable')

const selectEverythingState = () => state => state.everything
const selectEverythingData = () => createSelector(
  selectEverythingState(),
  subState => {
    if (subState.everythingData && isImmutable(subState.everythingData)) {
      return subState.everythingData.asMutable({deep: true})
    }
    return subState.everythingData
  }
)
const selectEverythingLoading = () => createSelector(selectEverythingState(), subState => subState.fetching)
const selectEverythingError = () => createSelector(selectEverythingState(), subState => subState.error)

export {
  selectEverythingState,
  selectEverythingData,
  selectEverythingError,
  selectEverythingLoading,
}
