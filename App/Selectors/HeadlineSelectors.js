import { createSelector } from 'reselect'

const selectHeadlineState = () => state => state.headline
const selectHeadlineData = () => createSelector(selectHeadlineState(), subState => subState.headlineData)
const selectHeadlineLoading = () => createSelector(selectHeadlineState(), subState => subState.fetching)
const selectHeadlineError = () => createSelector(selectHeadlineState(), subState => subState.error)

export {
  selectHeadlineState,
  selectHeadlineData,
  selectHeadlineError,
  selectHeadlineLoading,
}
