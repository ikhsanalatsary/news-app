import { createSelector } from 'reselect'

const selectNewsState = () => state => state.news
const selectNewsData = () => createSelector(selectNewsState(), subState => subState.newsData)
const selectNewsLoading = () => createSelector(selectNewsState(), subState => subState.fetching)
const selectNewsError = () => createSelector(selectNewsState(), subState => subState.error)

export {
  selectNewsState,
  selectNewsData,
  selectNewsError,
  selectNewsLoading,
}
