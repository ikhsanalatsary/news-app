import { createSelector } from 'reselect'

const selectSourceState = () => state => state.source
const selectSourceData = () => createSelector(selectSourceState(), subState => subState.sourceData)
const selectSourceLoading = () => createSelector(selectSourceState(), subState => subState.fetching)
const selectSourceError = () => createSelector(selectSourceState(), subState => subState.error)

export {
  selectSourceState,
  selectSourceData,
  selectSourceError,
  selectSourceLoading,
}
