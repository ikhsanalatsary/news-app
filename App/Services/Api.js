// a library to wrap and simplify api calls
import apisauce from 'apisauce'
import { CancelToken } from 'axios'
import Config from 'react-native-config'

// our "constructor"
const create = (baseURL = Config.BASE_ENDPOINT) => {
  // ------
  // STEP 1
  // ------
  //
  // Create and configure an apisauce-based api object.
  //
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
      'X-Api-Key': Config.NEWS_API_KEY,
    },
    // 10 second timeout...
    timeout: 10000,
  })

  // ------
  // STEP 2
  // ------
  //
  // Define some functions that call the api.  The goal is to provide
  // a thin wrapper of the api layer providing nicer feeling functions
  // rather than "get", "post" and friends.
  //
  // I generally don't like wrapping the output at this level because
  // sometimes specific actions need to be take on `403` or `401`, etc.
  //
  // Since we can't hide from that, we embrace it by getting out of the
  // way at this level.
  //
  const getRoot = () => api.get('')
  const getRate = () => api.get('rate_limit')
  const getUser = (username) => api.get('search/users', { q: username })
  const getTopHeadline = (params, config = {}) => {
    const endpoint = '/v2/top-headlines'
    return api.get(endpoint, params, config).then(checkStatus)
  }
  const getEverything = (params, config = {}) => {
    const endpoint = '/v2/everything'
    return api.get(endpoint, params, config).then(checkStatus)
  }
  const getArticles = (params, config = {}) => {
    const endpoint = '/v1/articles'
    return api.get(endpoint, params, config).then(checkStatus)
  }
  const getSources = (params, config = {}) => {
    const endpoint = '/v2/sources'
    return api.get(endpoint, params, config).then(checkStatus)
  }

  // ------
  // STEP 3
  // ------
  //
  // Return back a collection of functions that we would consider our
  // interface.  Most of the time it'll be just the list of all the
  // methods in step 2.
  //
  // Notice we're not returning back the `api` created in step 1?  That's
  // because it is scoped privately.  This is one way to create truly
  // private scoped goodies in JavaScript.
  //
  return {
    // a list of the API functions from step 2
    getRoot,
    getRate,
    getUser,
    getTopHeadline,
    getEverything,
    getArticles,
    getSources,
  }

  /**
   * Checks if a network request came back fine, and throws an error if not
   *
   * @param  {object} response   A response from a network request
   *
   * @return {object|undefined} Returns either the response, or throws an error
   */
  function checkStatus (response) {
    if (response.status >= 200 && response.status <= 300) {
      return response
    }
    const error = new Error(response.message)
    error.response = response
    throw error
  }
}

// let's return back our create method as the default.
export default {
  create,
  CancelToken,
}
